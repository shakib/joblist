
<form method="post" action="{{url('add/job/data')}}" method="POST" id="addJobForm" enctype="multipart/form-data">
    @csrf
    <div id="create">
        <button type="submit" class="btn btn-md myjobBtn"  ><i class="fa fa-plus-circle" aria-hidden="true"></i> New joblist</button>
    </div>
    <div id="newJob">
        <!--new joblist will place here when button is pressed-->
    </div>
    <div  id="joblist">
        <div class="card" id="getcard">
            <!-- Card -->
            <div class="card" id="mycard" style="padding: 2px;">
                <div class="card-body" style="padding: 0px;">
                    <div class="form-row input-group align-items-center d-lg-inline-flex justify-content-center">
                        <a style="width:4%; text-align: center;">Sl 01</a>
                        <input type="text" name="customer_name" id="prime" placeholder="Customer Name"style="width:10%">

                        <input type="text"  name="company_name"  id="prime" placeholder="Company Name"style="width:10%">

                        <input type="text"  name="address" id="prime" placeholder="Address"style="width:17%">

                        <input type="text" name="postcode" id="prime" placeholder="Post Code" style="width:5%">

                        <input type="text" name="land_number" id="prime" placeholder="Land Number" style="width:12%">

                        <input type="text" name="mobile_number" id="prime" placeholder="Mobile Number" style="width:12%">

                        <input type="email" name="email" id="prime" placeholder="email ID"style="width:12%">

                        <input type="url" name="website" id="prime" placeholder="website"style="width:10%">

                        <div id="create" style="width:8%;">
                            {{--<button type="button" class="btn btn-sm mynestBtn" onClick="createNest()"><i class="fa fa-plus-circle" aria-hidden="true"></i> Nest joblist</button>--}}
                        </div>

                    </div>
                    <div id="nest">
                        <div class="card" id="mycard2">
                            <div class="card-body" style="padding: 1px;">
                                <div class="form-row input-group align-items-center d-lg-inline-flex justify-content-center">
                                    <a style="width:5%; text-align: center;">Job 01</a>

                                    <input id="time"  value="{{ date('d/m/Y ') }}" style="background-color:#F5F5F5; font-size: 16px; width: 5%; height:25px;"></input>
                                    <input type="text" name="quantity" placeholder="quantity" style="width: 10%">
                                    <input type="date" style="width: 10%" name="date">
                                    <input type="text" name="description" placeholder="Job Description" style="width:50%">
                                    <select onchange="jobtype(mycard2,value)" name="jobtype_id" style="width: 10%;">
                                        <option value="" selected disabled hidden>Job type</option>
                                        @foreach ($job_types as $job_type)
                                            <option  value="{{$job_type->id}}">{{$job_type->name}}</option>
                                        @endforeach
                                    </select>

                                    <select id="colorlist" onchange="changecolor(mycard2,value)" name="jobstatus_id" style="width: 10%;">

                                        <option selected disabled hidden value="grey">Job Status</option>
                                        @foreach ($job_statuses as $job_status)
                                            <option value="{{$job_status->id}}">{{$job_status->name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="form-row input-group align-items-center d-lg-inline-flex justify-content-center">
                                    <select id="color_me" name="quotation_id" style="width: 8%">
                                        <option  disabled selected hidden style="background-color: #e6e7e8;" >Quoatation</option>
                                        @foreach ($quotations as $quotation)
                                            <option class="{{$quotation->id==1 ? 'green':'orange'}}" value="{{$quotation->id}}">{{$quotation->name}}</option>
                                        @endforeach

                                    </select>

                                    <input type="text" name="price"  placeholder="price" style="width:5%">
                                    {{--<input type="text" placeholder="0.00" style="width: 4%">--}}

                                    <select id="border_me" class="greenbord" name="payment_id" onchange="showme(duepay,value)" style="width: 10%;">
                                        <option selected disabled hidden >Payment</option>
                                        @foreach ($payments as $payment)
                                            <option class="{{ (($payment->id==5) ?'purpbord':(( $payment->id==7) ?'purpbord':(($payment->id==8 )?'orangebord':(( $payment->id==6) ?'megbord':'greenbord'))))}} " value="{{$payment->id}}" value2="45%">{{$payment->name}}</option>
                                        @endforeach
                                        {{--$payment->id==5 ?'purpbord': $payment->id==6 ? 'megbord': $payment->id==7 ? 'greenbord': $payment->id==8 ? 'orangebord':'greenbord'--}}
                                    </select>
                                    <input type="text" placeholder="due" id="duepay">

                                    <select id="design_stat" name="designstatus_id" class="redbord" style="width: 10%;">
                                        <option selected disabled hidden >Design Status</option>
                                        @foreach ($design_statuses as $design_status)
                                            <option class="{{ (($design_status->id==1) ?'redbord':(( $design_status->id==2) ?'yellowbord':(($design_status->id==3 )?'orangebord':(( $design_status->id==4) ?'greenbord':'purpbord'))))}}" value="{{$design_status->id}}">{{$design_status->name}}</option>
                                        @endforeach
                                    </select>

                                    <input type="text" id="remarks" name="remarks" placeholder="Remarks" style="width:40%;">
                                    <select id="invoice" name="invoice_id" class="redbord" style="width: 8%">
                                        <option selected disabled hidden>Invoice</option>
                                        @foreach ($invoices as $invoice)
                                            <option class="{{(($invoice->id==1) ?'redbord':(( $invoice->id==2) ?'orangebord':'greenbord'))}}" value="{{$invoice->id}}">{{$invoice->name}}</option>
                                        @endforeach

                                    </select>

                                    <input type="image" id="imgbtninv" src="{{ asset('img/invoice.png') }}" border="0" alt="Submit" style="width:30px;height:30px; border-right: none;">

                                    <input type="image" id="imgbtn" src="{{ asset('img/mailcus.png') }}" border="0" alt="Submit" style="width:30px;height:30px;border-right: none;">

                                    <input type="image" id="imgbtn" src="{{ asset('img/mailus.png') }}" border="0" alt="Submit" style="width:30px;height:30px;border-right: none;">

                                    <input type="image" id="imgbtn" src="{{ asset('img/save.png') }}" border="0" alt="Submit" style="width:30px;height:30px;border-right: none;">

                                    <input type="image" id="imgbtn" src="{{ asset('img/delete.png') }}" border="0" alt="Submit" style="width:30px;height:30px;border-right: none;">

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</form>