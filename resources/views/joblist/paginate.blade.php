@include('joblist/createjob')
@php($sl=1 )
<hr>
@foreach($jobs as $job)
    <div  id="joblist">
        <div class="card" id="mycard" style="padding: 2px;">
            <div class="card-body" style="padding: 0px;">
                <div class="form-row input-group align-items-center d-lg-inline-flex justify-content-center">
                    <a style="width:4%; text-align: center;">{{$sl++}}</a>
                    <input type="text" value="{{$job->customer_name}}" name="customer_name" id="prime" placeholder="Customer Name"style="width:10%">

                    <input type="text" value="{{$job->company_name}}"  name="company_name"  id="prime" placeholder="Company Name"style="width:10%">

                    <input type="text" value="{{$job->address}} " name="address" id="prime" placeholder="Address"style="width:17%">

                    <input type="text" value="{{$job->postcode}}"  name="postcode" id="prime" placeholder="Post Code" style="width:5%">

                    <input type="text" value="{{$job->land_number}}" name="land_number" id="prime" placeholder="Land Number" style="width:12%">

                    <input type="text"  value="{{$job->mobile_number}} " name="mobile_number" id="prime" placeholder="Mobile Number" style="width:12%">

                    <input type="email" value="{{$job->mobile_number}}"  name="email" id="prime" placeholder="email ID"style="width:12%">

                    <input type="url"  value="{{$job->website}}" name="website" id="prime" placeholder="website"style="width:10%">

                    <div id="create" style="width:8%;">
                        <a href="{{url("get/nested-job")}}"  type="button" class="btn btn-sm mynestBtn" id="nestedJob" data-id="{{$job->id}} data-target="#nestedJobModal"><i class="fa fa-plus-circle" aria-hidden="true"></i> Nest joblist</a>
                    </div>

                </div>
                <div id="nest">
                    <div class="card" id="mycard2">
                        <div class="card-body" style="padding: 1px;">
                            <div class="form-row input-group align-items-center d-lg-inline-flex justify-content-center">
                                <a style="width:5%; text-align: center;"></a>
                                {{--<a id="time" style="background-color:#F5F5F5; font-size: 16px; width: 5%; height:25px;"></a>--}}
                                <input type="text" value="{{ date('d/m/Y ') }}" style="font-size: 14px; width: 5%; height:25px;">
                                <input type="text" name="quantity" value="{{$job->quantity}}" placeholder="quantity" style="width: 10% ; text-align: center;">
                                <input type="date" style="width: 10%" value="{{$job->date}}" name="date">
                                <input type="text" name="description" value="{{$job->description}}" placeholder="Job Description" style="width:50%;text-align: center">
                                <select onchange="jobtype(mycard2,value)" name="jobtype_id" style="width: 10%;">
                                    {{--<option selected disabled hidden>Job Type</option>--}}
                                    <option  value="1">{{$job->jobtype->name}}</option>
                                    @foreach ($job_types as $job_type)
                                        <option  value="{{$job_type->id}}">{{$job_type->name}}</option>
                                    @endforeach
                                </select>
                                <select id="colorlist" onchange="changecolor(mycard2,value)" name="jobstatus_id" style="width: 10%;">
                                    <option value="{{$job->jobstatus->id}}">{{$job->jobstatus->name}}</option>
                                    @foreach ($job_statuses as $job_status)
                                        <option value="{{$job_status->id}}">{{$job_status->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-row input-group align-items-center d-lg-inline-flex justify-content-center">
                                <select  id="color_me" name="quotation" style="width: 8%">
                                    @foreach ($quotations as $quotation)
                                        <option class="{{$quotation->id==1 ? 'green':'orange'}}" value="{{$quotation->id}}">{{$quotation->name}}</option>
                                    @endforeach
                                    <option class="{{$job->quotation->id==1 ? 'green':'orange'}}" selected disabled hidden  value="{{$job->quotation->id}}">{{$job->quotation->name}}</option>
                                </select>

                                <input type="text" name="price" value="£{{$job->price}}" placeholder="price" style="width:5%">
                                {{--<input type="text" placeholder="0.00" style="width: 4%">--}}

                                <select id="border_me" class="greenbord" name="payment" onchange="showme(duepay,value)" style="width: 10%;">
                                    {{--<option selected disabled hidden >Payment</option>--}}
                                    <option class="{{ (($job->payment->id==5) ?'purpbord':(( $job->payment->id==7) ?'purpbord':(($job->payment->id==8 )?'orangebord':(( $job->payment->id==6) ?'megbord':'greenbord'))))}}">{{$job->payment->name}}</option>
                                    @foreach ($payments as $payment)
                                        <option class="{{ (($payment->id==5) ?'purpbord':(( $payment->id==7) ?'purpbord':(($payment->id==8 )?'orangebord':(( $payment->id==6) ?'megbord':'greenbord'))))}}" value="1">{{$payment->name}}</option>
                                    @endforeach

                                </select>
                                <input type="text" placeholder="due" id="duepay">

                                <select id="design_stat" name="designstatus_id" class="redbord" style="width: 10%;">
                                    {{--<option selected disabled hidden >Design Status</option>--}}
                                    <option class="{{ (($job->designstatus->id==1) ?'redbord':(( $job->designstatus->id==2) ?'yellowbord':(($job->designstatus->id==3 )?'orangebord':(( $job->designstatus->id==4) ?'greenbord':'purpbord'))))}}" value="1">{{$job->designstatus->name}}</option>
                                    @foreach ($design_statuses as $design_status)
                                        <option class="{{ (($design_status->id==1) ?'redbord':(( $design_status->id==2) ?'yellowbord':(($design_status->id==3 )?'orangebord':(( $design_status->id==4) ?'greenbord':'purpbord'))))}}" value="{{$design_status->id}}">{{$design_status->name}}</option>
                                    @endforeach

                                </select>

                                <input type="text" id="remarks" name="remarks" placeholder="Remarks" value="{{$job->remarks}}" style="width:40%;text-align: center">
                                <select id="invoice" name="invoice" class="redbord" style="width: 8%">
                                    <option class="{{(($job->invoice->id==1) ?'redbord':(( $job->invoice->id==2) ?'orangebord':'greenbord'))}}" value="{{$job->invoice->id}}" value="1">{{$job->invoice->name}}</option>
                                    @foreach ($invoices as $invoice)
                                        <option class="{{(($invoice->id==1) ?'redbord':(( $invoice->id==2) ?'orangebord':'greenbord'))}}" value="{{$invoice->id}}">{{$invoice->name}}</option>
                                    @endforeach

                                </select>

                                <input type="image" id="imgbtninv" src="{{ asset('img/invoice.png') }}" border="0" alt="Submit" style="width:30px;height:30px; border-right: none;">

                                <input type="image" id="imgbtn" src="{{ asset('img/mailcus.png') }}" border="0" alt="Submit" style="width:30px;height:30px;border-right: none;">

                                <input type="image" id="imgbtn" src="{{ asset('img/mailus.png') }}" border="0" alt="Submit" style="width:30px;height:30px;border-right: none;">

                                <input type="image" id="imgbtn" src="{{ asset('img/save.png') }}" border="0" alt="Submit" style="width:30px;height:30px;border-right: none;">

                                <input type="image" id="imgbtn" src="{{ asset('img/delete.png') }}" border="0" alt="Submit" style="width:30px;height:30px;border-right: none;">

                            </div>
                        </div>
                    </div>
                </div>

                {{--Show  nested job start--}}


                <div id="createnest">
                    <!--nested elemnet will place here when pressed nest joblist-->
                    @foreach ($njobs as $njob)
                        @if($job->id==$njob->joblist_id)
                            <div class="card-body" style="padding: 1px;" >
                                <div class="form-row input-group align-items-center d-lg-inline-flex justify-content-center">
                                    <a style="width:5%; text-align: center;">{{$njob->id }}</a>

                                    <input type="text" value="{{ date('d/m/Y ') }}" style="font-size:14px; width: 7%; ">
                                    <input type="text" value="{{$njob->quantity }}" name="quantity" placeholder="quantity" style="width: 6%;text-align: center">
                                    <input type="date" value="{{$njob->date}}" style="width:10% ;font-size:14px;" name="date">
                                    <input type="text" value="{{$njob->description }}" name="description" placeholder="Job Description" style="width:50%;text-align: center">
                                    <select onchange="jobtype(mycard2,value)" name="jobtype_id" style="width: 10%;">
                                        <option value="" selected disabled hidden>{{$njob->jobtype->name}}</option>
                                        @foreach ($job_types as $job_type)
                                            <option  value="{{$job_type->id}}">{{$job_type->name}}</option>
                                        @endforeach
                                    </select>

                                    <select id="colorlist" onchange="changecolor(mycard2,value)" name="jobstatus_id" style="width: 10%;text-align: center">

                                        <option selected disabled hidden value="grey">{{$njob->jobstatus->name}}</option>
                                        @foreach ($job_statuses as $job_status)
                                            <option value="{{$job_status->id}}">{{$job_status->name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="form-row input-group align-items-center d-lg-inline-flex justify-content-center">
                                    <select id="color_me" name="quotation_id" style="width: 9%;text-align: center">
                                        <option  disabled selected hidden style="background-color: #e6e7e8;" >{{$njob->quotation->name}}</option>
                                        @foreach ($quotations as $quotation)
                                            <option class="{{$quotation->id==1 ? 'green':'orange'}}" value="{{$quotation->id}}">{{$quotation->name}}</option>
                                        @endforeach

                                    </select>

                                    <input type="text" name="price" value="{{$njob->price}}"  placeholder="price" style="width:5%;text-align: center">
                                    {{--<input type="text" placeholder="0.00" style="width: 4%">--}}

                                    <select id="border_me" class="greenbord" name="payment_id" onchange="showme(duepay,value)" style="width: 10%;text-align: center">
                                        <option selected disabled hidden >{{$njob->payment->name}}</option>
                                        @foreach ($payments as $payment)
                                            <option class="{{ (($payment->id==5) ?'purpbord':(( $payment->id==7) ?'purpbord':(($payment->id==8 )?'orangebord':(( $payment->id==6) ?'megbord':'greenbord'))))}} " value="{{$payment->id}}" value2="45%">{{$payment->name}}</option>
                                        @endforeach
                                        {{--$payment->id==5 ?'purpbord': $payment->id==6 ? 'megbord': $payment->id==7 ? 'greenbord': $payment->id==8 ? 'orangebord':'greenbord'--}}
                                    </select>
                                    <input type="text" placeholder="due" id="duepay">

                                    <select id="design_stat" name="designstatus_id" class="redbord" style="width: 10%;text-align: center">
                                        <option selected disabled hidden >{{$njob->designstatus->name}}</option>
                                        @foreach ($design_statuses as $design_status)
                                            <option class="{{ (($design_status->id==1) ?'redbord':(( $design_status->id==2) ?'yellowbord':(($design_status->id==3 )?'orangebord':(( $design_status->id==4) ?'greenbord':'purpbord'))))}}" value="{{$design_status->id}}">{{$design_status->name}}</option>
                                        @endforeach
                                    </select>

                                    <input type="text" id="remarks" name="remarks" value="{{$njob->remarks}}" placeholder="Remarks" style="width:40%;text-align: center">
                                    <select id="invoice" name="invoice_id" class="redbord" style="width: 8%; text-align: center">
                                        <option selected disabled hidden>{{$njob->invoice->name}}</option>
                                        @foreach ($invoices as $invoice)
                                            <option class="{{(($invoice->id==1) ?'redbord':(( $invoice->id==2) ?'orangebord':'greenbord'))}}" value="{{$invoice->id}}">{{$invoice->name}}</option>
                                        @endforeach

                                    </select>

                                    <input type="image" id="imgbtninv" src="{{ asset('img/invoice.png') }}" border="0" alt="Submit" style="width:30px;height:30px; border-right: none;">

                                    <input type="image" id="imgbtn" src="{{ asset('img/mailcus.png') }}" border="0" alt="Submit" style="width:30px;height:30px;border-right: none;">

                                    <input type="image" id="imgbtn" src="{{ asset('img/mailus.png') }}" border="0" alt="Submit" style="width:30px;height:30px;border-right: none;">

                                    <input type="image" id="imgbtn" src="{{ asset('img/save.png') }}" border="0" alt="Submit" style="width:30px;height:30px;border-right: none;">

                                    <input type="image" id="imgbtn" src="{{ asset('img/delete.png') }}" border="0" alt="Submit" style="width:30px;height:30px;border-right: none;">

                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
                {{--Show  nested job end --}}
            </div>
        </div>
    </div>
    {{--Nested modal--}}

    <br>
@endforeach
{{$jobs->links()}}
<div id="getalldata" data-url="{{url("get/job/data")}}"> </div>
{{--<div id="getalldatabypagination" data-url="{{url("get/job/data/by/pagination")}}"></div>--}}

<div class="modal fade" id="nestedJobModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog " style="margin-left:20%;" role="document">
        <form action="{{url('add/nested-job')}}" method="POST" id="addNestedJobForm">
            @csrf
            <div class="modal-content"  style="width:950px;  margin: auto; ">
                <div class="modal-header">
                    <h5 class="modal-title" id="jobTitle">  </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body  ">


                    <input type="hidden" value="" id="nestedid" name="id">


                    <div class="card-body" style="padding: 1px;" >
                        <div class="form-row input-group align-items-center d-lg-inline-flex justify-content-center">
                            <a style="width:5%; text-align: center;"></a>

                            <input type="text" value="{{ date('d/m/Y ') }}" style="font-size:12px; width: 8%; height:25px;">
                            <input type="text" name="quantity" placeholder="quantity" style="width: 6%">
                            <input type="date" style="width: 10% ;font-size:13px;" name="date">
                            <input type="text" name="description" placeholder="Job Description" style="width:50%">
                            <select onchange="jobtype(mycard2,value)" name="jobtype_id" style="width: 10%;">
                                <option value="" selected disabled hidden>Job type</option>
                                @foreach ($job_types as $job_type)
                                    <option  value="{{$job_type->id}}">{{$job_type->name}}</option>
                                @endforeach
                            </select>

                            <select id="colorlist" onchange="changecolor(mycard2,value)" name="jobstatus_id" style="width: 10%;">

                                <option selected disabled hidden value="grey">Job Status</option>
                                @foreach ($job_statuses as $job_status)
                                    <option value="{{$job_status->id}}">{{$job_status->name}}</option>
                                @endforeach

                            </select>
                        </div>
                        <div class="form-row input-group align-items-center d-lg-inline-flex justify-content-center">
                            <select id="color_me" name="quotation_id" style="width: 9%">
                                <option  disabled selected hidden style="background-color: #e6e7e8;" >Quoatation</option>
                                @foreach ($quotations as $quotation)
                                    <option class="{{$quotation->id==1 ? 'green':'orange'}}" value="{{$quotation->id}}">{{$quotation->name}}</option>
                                @endforeach

                            </select>

                            <input type="text" name="price"  placeholder="price" style="width:5%">
                            {{--<input type="text" placeholder="0.00" style="width: 4%">--}}

                            <select id="border_me" class="greenbord" name="payment_id" onchange="showme(duepay,value)" style="width: 10%;">
                                <option selected disabled hidden >Payment</option>
                                @foreach ($payments as $payment)
                                    <option class="{{ (($payment->id==5) ?'purpbord':(( $payment->id==7) ?'purpbord':(($payment->id==8 )?'orangebord':(( $payment->id==6) ?'megbord':'greenbord'))))}} " value="{{$payment->id}}" value2="45%">{{$payment->name}}</option>
                                @endforeach
                                {{--$payment->id==5 ?'purpbord': $payment->id==6 ? 'megbord': $payment->id==7 ? 'greenbord': $payment->id==8 ? 'orangebord':'greenbord'--}}
                            </select>
                            <input type="text" placeholder="due" id="duepay">

                            <select id="design_stat" name="designstatus_id" class="redbord" style="width: 10%;">
                                <option selected disabled hidden >Design Status</option>
                                @foreach ($design_statuses as $design_status)
                                    <option class="{{ (($design_status->id==1) ?'redbord':(( $design_status->id==2) ?'yellowbord':(($design_status->id==3 )?'orangebord':(( $design_status->id==4) ?'greenbord':'purpbord'))))}}" value="{{$design_status->id}}">{{$design_status->name}}</option>
                                @endforeach
                            </select>

                            <input type="text" id="remarks" name="remarks" placeholder="Remarks" style="width:40%;">
                            <select id="invoice" name="invoice_id" class="redbord" style="width: 8%">
                                <option selected disabled hidden>Invoice</option>
                                @foreach ($invoices as $invoice)
                                    <option class="{{(($invoice->id==1) ?'redbord':(( $invoice->id==2) ?'orangebord':'greenbord'))}}" value="{{$invoice->id}}">{{$invoice->name}}</option>
                                @endforeach

                            </select>

                            <input type="image" id="imgbtninv" src="{{ asset('img/invoice.png') }}" border="0" alt="Submit" style="width:30px;height:30px; border-right: none;">

                            <input type="image" id="imgbtn" src="{{ asset('img/mailcus.png') }}" border="0" alt="Submit" style="width:30px;height:30px;border-right: none;">

                            <input type="image" id="imgbtn" src="{{ asset('img/mailus.png') }}" border="0" alt="Submit" style="width:30px;height:30px;border-right: none;">

                            <input type="image" id="imgbtn" src="{{ asset('img/save.png') }}" border="0" alt="Submit" style="width:30px;height:30px;border-right: none;">

                            <input type="image" id="imgbtn" src="{{ asset('img/delete.png') }}" border="0" alt="Submit" style="width:30px;height:30px;border-right: none;">

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit"    class="btn btn-primary" >Add</button>
                </div>
            </div>
        </form>
    </div>
</div>

{{--Nested modal--}}

