$(function() {
   // return getJobdata();
    function getJobdata(){
        var url =$("#getalldata").data("url");
        console.log(url);
        $.ajax({
            url:url,
            type:"get",
            dataType:"html",
            success:function (response) {
                //console.log(response);
                $("#showalldata").html(response)
            },
        });
    }
    $(document).on("click", "#nestedJob", function (arg) {
        arg.preventDefault();
        var id = $(this).data("id");
        var url = $(this).attr("href");
        // console.log(id + "-----" + url);


        $.ajax({
            url: url,
            data: {id: id},
            type: "get",
            dataType: "json",
            success: function (response) {
                if ($.isEmptyObject(response) != null) {
                    $("#nestedJobModal").modal("show");
                    $("#jobTitle").text("Add Nested Job to "+ response.company_name);
                    $("#nestedid").val( response.id);
                    // $("#addNestedJobForm").on("submit",function(l) {
                    //
                    //     l.preventDefault();
                    //     var form = $(this);
                    //     var url = form.attr('action');
                    //     var method =form.attr('method');
                    //
                    //     var data = form.serialize();
                    //
                    //     //   console.log(data);
                    //
                    //     $.ajax({
                    //         url:url,
                    //         data:data,
                    //         type:method,
                    //         dataType:"JSON",
                    //
                    //         beforeSend:function () {
                    //
                    //             // $(".gif").fadeIn();
                    //
                    //
                    //         },
                    //         success:function (data) {
                    //             // alert(data);
                    //             // console.log(data)
                    //             if(data=="success"){
                    //                 $("#nestedJobModal").modal("hide");
                    //                 form[0].reset();
                    //                 return getJobdata();
                    //
                    //
                    //             }
                    //         },
                    //         // error: function(XMLHttpRequest, textStatus, errorThrown) {
                    //         //     // alert("some error");
                    //         //     console.log(XMLHttpRequest)
                    //         // },
                    //         complete:function () {
                    //             // alert('complete ...');
                    //             // $(".gif").fadeOut();
                    //         },
                    //     });

                    // });
                }
                else{
                        console.log("error");
                    }
            }
        })
    });

    $("#addNestedJobForm").on("submit",function(l) {

        l.preventDefault();
        var form = $(this);
        var url = form.attr('action');
        var method =form.attr('method');

        var data = form.serialize();

     //   console.log(data);

        $.ajax({
            url:url,
            data:data,
            type:method,
            dataType:"JSON",

            beforeSend:function () {

               // $(".gif").fadeIn();


            },
            success:function (data) {
              // alert(data);
            // console.log(data)
                if(data=="success"){
                    $("#nestedJobModal").modal("hide");
                    form[0].reset();
                    return getJobdata();


                }
            },
            // error: function(XMLHttpRequest, textStatus, errorThrown) {
            //     // alert("some error");
            //     console.log(XMLHttpRequest)
            // },
            complete:function () {
                // alert('complete ...');
                // $(".gif").fadeOut();
            },
        });

    });




    $("#addJobForm").on("submit",function(l){
        l.preventDefault();
        var form = $(this);
        var url = form.attr('action');
        var method =form.attr('method');

        var data = form.serialize();
        // console.log(data)
        $.ajax({
            url:url,
            data:data,
            type:method,
            dataType:"JSON",

            beforeSend:function () {
              alert("creating ...")
                //  $(".gif").fadeIn();
            },
            success:function (data) {
               // console.log(data);
                if (data == "success") {
                    //$("#addCustomerModal").modal("hide");
                   // console.log(data);
                    //swal("Great", "Job Added", "success");
                    form[0].reset();
                    return getJobdata();

                }
            },
            complete:function () {
                alert("completed ....")
            // $(".gif").fadeOut();
          },

        })

    });

    //
    // $(document).on("click",".pagination li a ",function (e) {
    //     e.preventDefault();
    //     var page = $(this).attr("href");
    //     var pagenumber = page.split("?page=")[1];
    //    // console.log(pagenumber);
    //      return getpagination(pagenumber)
    // });
    // function getpagination(pagenumber) {
    //     var url = $("#getalldatabypagination").data("url") + "?page="+pagenumber;
    //     //console.log(url);
    //     $.ajax({
    //         url:url,
    //         type:"get",
    //         dataType:"HTML",
    //         success:function (response) {
    //             //console.log(response);
    //               $("#showalldata").html(response)
    //         }
    //
    //     })
    // }
    //
    //

});