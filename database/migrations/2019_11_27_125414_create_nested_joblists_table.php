<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNestedJoblistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nested_joblists', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('joblist_id')->unsigned();
            $table->integer('quantity');
            $table->string('date');
            $table->string('description');
            $table->integer('jobtype_id')->unsigned()->nullable()->index();
            $table->integer('jobstatus_id')->unsigned()->nullable()->index();
            $table->integer('quotation_id')->unsigned()->nullable()->index();
            $table->string('price');
            $table->integer('payment_id')->unsigned()->nullable()->index();
            $table->integer('designstatus_id')->unsigned()->nullable()->index();
            $table->string('remarks');
            $table->integer('invoice_id')->unsigned()->nullable()->index();;
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nested_joblists');
    }
}
