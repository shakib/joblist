<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Joblist extends Model
{
    protected $fillable = ['customer_name','company_name', 'address', 'postcode','land_number','mobile_number','email','website','quantity',
        'date','description','jobtype_id','jobstatus_id','quotation_id','payment_id','designstatus_id','remarks','invoice_id','price'];
    
    
    public function jobtype()
    {
        return $this->belongsTo('App\Jobtype');
    
    }
    public function jobstatus()
    {
        return $this->belongsTo('App\Jobstatus');
        
    }
    public function designstatus()
    {
        return $this->belongsTo('App\Designstatus');
        
    }
    public function quotation()
    {
        return $this->belongsTo('App\Quotation');
        
    }
    public function payment()
    {
        return $this->belongsTo('App\Payment');
        
    }
    public function invoice()
    {
        return $this->belongsTo('App\Invoice');
        
    }
    
}
