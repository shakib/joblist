<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'name',
    ];
    
    public function joblists()
    {
        return $this->hasMany('App\Joblist');
    }
}
