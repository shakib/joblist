<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['customer_name','company_name', 'address', 'postcode','land_number','mobile_number','email','website'];
    
}
