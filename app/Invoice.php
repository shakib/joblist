<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'name',
    ];
    
    public function joblists()
    {
        return $this->hasMany('App\Joblist');
    }
    public function nested_joblists()
    {
        return $this->hasMany('App\NestedJoblist');
    }
}
