<?php

namespace App\Http\Controllers;

use App\Joblist;
use App\NestedJoblist;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JobController extends Controller
{
//    public function index()
//    {
//        return view('joblist/index');
//    }
//

//    public function jobStore(Request $request)
//    {
//        $input = $request->all();
//       // dd($input);
//
//        Joblist::create($input);
//        //dd($m);
//        //return redirect('');
//        return redirect()->back();
//    }
    
    public function addJob(Request $request)
    {
        $insert = Joblist::insert([
            'customer_name'=>$request->customer_name,
            'company_name'=>$request->company_name,
            'address'=>$request->address,
            'postcode'=>$request->postcode,
            'land_number'=>$request->land_number,
            'mobile_number'=>$request->mobile_number,
            'email'=>$request->email,
            'website'=>$request->website,
            "quantity"=>$request->quantity,
            "date"=>$request->date,
            "description"=>$request->description,
            "jobtype_id"=>$request->jobtype_id,
            "jobstatus_id"=>$request->jobstatus_id,
            "quotation_id"=>$request->quotation_id,
            "payment_id"=>$request->payment_id,
            "designstatus_id"=>$request->designstatus_id,
            "remarks"=>$request->remarks,
            "invoice_id"=>$request->invoice_id,
            "price"=>$request->price,
            "created_at" => Carbon::now()
    
        ]);
    
        if($insert){
            
            return response()->json('success');
        }else{
            return response()->json('failure');
        
        }
    }
//    public function jobRetrieve()
//    {
////        $jobs = DB::table('joblists')->get();
//        $jobs = DB::table('joblists')->get();
//        $job_types = DB::table('jobtypes')->get();
//        $job_statuses = DB::table('jobstatuses')->get();
//        $quotations = DB::table('quotations')->get();
//        $payments =  DB::table('payments')->get();
//        $design_statuses =  DB::table('designstatuses')->get();
//        $invoices =  DB::table('invoices')->get();
//        //dd($jobs[0]->jobtype_id);
//        //testing
//        //dd($job_types[0]->id);
//        return view('joblist/index',compact('jobs','job_types','job_statuses','quotations','payments','design_statuses','invoices'));
//
//    }
//
    public function showNestedModal(Request $request)
    {
        $id = $request->id;
        $job = Joblist::findOrfail($id);
        if($job){
            return $job;
        }else{
            return response()->json('failure');
        
        }
        
    }
    
    public function addNested(Request $request)
    {
        //return $request;
        $insert = NestedJoblist::insert([
           "joblist_id"=>$request->id,
            "quantity"=>$request->quantity,
            "date"=>$request->date,
            "description"=>$request->description,
            "jobtype_id"=>$request->jobtype_id,
            "jobstatus_id"=>$request->jobstatus_id,
            "quotation_id"=>$request->quotation_id,
            "payment_id"=>$request->payment_id,
            "designstatus_id"=>$request->designstatus_id,
            "remarks"=>$request->remarks,
            "invoice_id"=>$request->invoice_id,
            "price"=>$request->price,
            "created_at" => Carbon::now()

        ]);
        if($insert){
            return response()->json('success');
        }else{
            return response()->json('failure');

        }
    
    }
    protected function getJobdata()
    {
        $jobs = Joblist::latest()->paginate(5);
        $job_types = DB::table('jobtypes')->get();
        $job_statuses = DB::table('jobstatuses')->get();
        $quotations = DB::table('quotations')->get();
        $payments =  DB::table('payments')->get();
        $design_statuses =  DB::table('designstatuses')->get();
        $invoices =  DB::table('invoices')->get();
        $njobs = NestedJoblist::all();
       // $data = Customer::latest()->paginate(5);
       // $sl=1;
        return view('joblist/index',compact('jobs','job_types','job_statuses','quotations','payments','design_statuses','invoices','njobs'));
    }
    protected function getpagination(){
        $jobs = Joblist::latest()->paginate(5);
        $job_types = DB::table('jobtypes')->get();
        $job_statuses = DB::table('jobstatuses')->get();
        $quotations = DB::table('quotations')->get();
        $payments =  DB::table('payments')->get();
        $design_statuses =  DB::table('designstatuses')->get();
        $invoices =  DB::table('invoices')->get();
        $njobs = NestedJoblist::latest()->paginate(5);
        return view("joblist/paginate",compact('jobs','job_types','job_statuses','quotations','payments','design_statuses','invoices','njobs'));
    }
    
    
}
