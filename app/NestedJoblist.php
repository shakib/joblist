<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NestedJoblist extends Model
{
    protected $fillable = ['quantity', 'date','description','jobtype_id','jobstatus_id','quotation_id','payment_id','designstatus_id','remarks','invoice_id','price'.'joblist_id'];
    
    
    public function jobtype()
    {
        return $this->belongsTo('App\Jobtype');
        
    }
    public function jobstatus()
    {
        return $this->belongsTo('App\Jobstatus');
        
    }
    public function designstatus()
    {
        return $this->belongsTo('App\Designstatus');
        
    }
    public function quotation()
    {
        return $this->belongsTo('App\Quotation');
        
    }
    public function payment()
    {
        return $this->belongsTo('App\Payment');
        
    }
    public function invoice()
    {
        return $this->belongsTo('App\Invoice');
        
    }



}
