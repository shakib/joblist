<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('job','JobController@index');
Route::post('job','jobController@jobStore')->name('job-store');
Route::get('job','jobController@jobRetrieve');


//using ajax
//show nested modal
Route::get('get/nested-job','jobController@showNestedModal');

// Add nested modal
    Route::post('add/nested-job','jobController@addNested');
    
    Route::get('get/job/data','jobController@getJobdata');
    
    // add job
Route::post('add/job/data','jobController@addJob');

//pagination
    Route::get('get/job/data/by/pagination','JobController@getpagination');

